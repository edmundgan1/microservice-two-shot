from django.shortcuts import render
from common.json import ModelEncoder
from .models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
# from wardrobe.api.wardrobe_api.views import LocationEncoder
# Create your views here.

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style",
        "color",
        "picture_url",
        "location"
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):

    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            href = content["location"]
            location = LocationVO.objects.get(import_href = href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {
                    "message":"invalid location id"
                }
            )

    hat = Hat.objects.create(**content)
    return JsonResponse(
        hat,
        encoder = HatListEncoder,
        safe = False,
    )


@require_http_methods(["DELETE"])
def api_delete_hat(request, pk):
    if request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
