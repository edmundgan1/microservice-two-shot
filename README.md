# Wardrobify

Team:

* Edmund Gan - Hats
* Elijah Sierra - Shoes

## How to Run
Once you have cloned the application from the gitlab, in the terminal run the following commands:
    1. docker volume create pgdata
    2. docker-compose build
    3. docker-compose up
Once the following three commands are ran in that order, the pollers should start running and data will be transmitted from the corresponding micro-services. Once models are created they will begin to populate the list for the corresponding micro-service. The application should be running and you should be able to  get there by including the link below in the browser.

    http://localhost:3000/

You should be able to navigate through the navigation bar on the top to add hats and shoes into the wardrobes.

You should also notice when you try to create hats and shoes you need a bin or a location. This can be added through insomnia using the wardrobe api's.

    - List bins http://localhost:8100/api/bins/
    - Get Bin Details http://localhost:8100/api/bins/<int:pk>/
    - Create a bin http://localhost:8100/api/bins/
    - Update a bin http://localhost:8100/api/bins/<int:pk>/
    - Delete a Bin http://localhost:8100/api/bins/<int:pk>/

    ```
    sample json used to create a bin

        {
        "closet_name": "secondary",
        "bin_number": "2",
        "bin_size": "10"
        }

    sample json used to update a bin

        {
        "closet_name": "secondary",
        "bin_number": "2",
        "bin_size": "15"
        }
    ```

    - List locations http://localhost:8100/api/locations/
    - Get location Details http://localhost:8100/api/locations/<int:pk>/
    - Create a location http://localhost:8100/api/locations/
    - Update a location http://localhost:8100/api/locations/<int:pk>/
    - Delete a location http://localhost:8100/api/locations/<int:pk>/

    ```
    sample json used to create a location

        {
        "closet_name": "One-Three",
        "section_number": "1",
        "shelf_number": "3"
        }

    sample json used to update a location

        {
            "closet_name": "One-Two",
            "section_number": 1,
            "shelf_number": 2
        }
        ```

## Design
![Alt text](Wardrobify_Diagram.png)

![Alt text](CRUDRouteDocumentation.png)

![Alt text](DefinedURLS.png)

## Shoes microservice

The shoe model in the shoes_rest folder is associated with all of the data pertaining to shoes in the wardrobe service, we can define the manufacturer, shoe model by name, color, a photo url of the shoe, as well as the bin the shoe can be found in. The binVO model is used as the value object because this is how the api will be able to access the information about data in the shoe microservice. The bin model is organized by a name and number, as well as the size of the bin.

___

ports:

    - List Shoes http://localhost:8080/api/shoes/
    - Get Shoe Details http://localhost:8080/api/shoes/<int:pk>/
    - Create a shoe http://localhost:8080/api/shoes/
    - Update Shoe http://localhost:8080/api/shoes/<int:pk>/
    - Delete Shoe http://localhost:8080/api/shoes/<int:pk>/

    ```
    Sample json to create a shoe model -

    {
     "manufacturer": "Nike",
     "name": "Dunk Lows",
     "color": "black on black",
     "photo_url": "google.com",
     "bin": "/api/locations/2/"
    }

    Sample json to update an existing model -

    {
	 "manufacturer": "Nike",
	 "name": "Dunk Lows",
	 "color": "Black on Black",
	 "photo_url": "google.com",
	 "bin": 1
    }

    ```

## Hats microservice

The hat model in the hat_rest folder is associated with all of the data pertaining to hats in the wardrobe service, we can define the hat model by fabric, style, color, and a picture url of the hat, as well as the loaction the hat can be found in. The LocationVO model is used as the value object because this is how the api will be able to access the information about data in the hat microservice. The location model is organized by a closet name and shelf number, as well as section number.

    -Get Hat Details http://localhost:8090/api/hats/<int:pk>/
    - Create a shoe http://localhost:8090/api/hats/
    - Update Shoe http://localhost:8090/api/hats/<int:pk>/
    - Delete Shoe http://localhost:8090/api/hats/<int:pk>/

    ```
    Sample json to create a hat model -

    {
        "fabric": "test",
        "style": "test",
        "color": "test",
        "picture_url": "test",
        "location": "/api/locations/1/"

    }
    
    ```
