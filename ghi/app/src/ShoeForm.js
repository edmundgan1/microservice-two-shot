import React from "react";

class ShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            color: '',
            manufacturer: '',
            photo_url: '',
            bin: '',
            bins: []
        }

        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleColorChange = this.handleColorChange.bind(this)
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this)
        this.handleUrlChange = this.handleUrlChange.bind(this)
        this.handleBinChange = this.handleBinChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        delete data.bins

        const url = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newShoe = await response.json()

            this.setState({
                name: '',
                color: '',
                manufacturer: '',
                photo_url: '',
                bin: '',
            })
        }
        window.location.reload()
    }

    handleNameChange(event){
        const value = event.target.value
        this.setState({name:value})
    }

    handleColorChange(event){
        const value = event.target.value
        this.setState({color:value})
    }

    handleManufacturerChange(event){
        const value = event.target.value
        this.setState({manufacturer:value})
    }

    handleUrlChange(event){
        const value = event.target.value
        this.setState({photo_url:value})
    }

    handleBinChange(event){
        const value = event.target.value
        this.setState({bin:value})
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/'
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            this.setState({bins: data.bins})
        }
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a New Shoe</h1>
                        <form id="create-shoes-form" onSubmit={this.handleSubmit}>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Shoe Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Shoe Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" id="color" name="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleManufacturerChange} value = {this.state.manufacturer} placeholder="Manufacturer" required type="text" id="manufacturer" name="manufacturer" className="form-control" />
                                <label htmlFor="manufacturer">Manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleUrlChange} value={this.state.photo_url} placeholder="Picture Url" required type="text" id="picture_url" name="picture_url" className="form-control" />
                                <label htmlFor="picture_url">Picture Url</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleBinChange} value={this.state.bin} required id="bin" name="bin" className="form-select">
                                    <option value="">Select Bin</option>
                                    {this.state.bins.map((bin) => {
                                        return(
                                        <option key={bin.id} value={bin.href}>
                                            {bin.closet_name}
                                        </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default ShoeForm
