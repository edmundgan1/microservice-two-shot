const DeleteHat = ((id) => {
    if (window.confirm("Confirm")) {
        fetch("http://localhost:8090/api/hats/" + id,
            { method: "delete" }).then(() => {

                window.location.reload()

            }).catch((err) => {
                console.log(err.message)
            })
    }
})

function HatList(props) {
    return (
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Fabric</th>
                            <th>Style</th>
                            <th>Color</th>
                            <th>Picture</th>
                            <th>Location</th>
                            <th>Delete?</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.hats.map(hat => {
                            return (
                                <tr key={hat.id}>
                                    <td>
                                        <img src={hat.picture_url} width="200" height="200" className="card-img-top" />
                                    </td>
                                    <td>{hat.fabric}</td>
                                    <td>{hat.style}</td>
                                    <td>{hat.color}</td>
                                    <td>{hat.location.closet_name}</td>
                                    <td>
                                        <button className="btn btn-lg btn-danger"
                                            onClick={() => { DeleteHat(hat.id) }} >Delete</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
    );
}

export default HatList;
