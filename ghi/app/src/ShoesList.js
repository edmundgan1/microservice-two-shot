const deleteShoes = async (id) => {

  fetch(`http://localhost:8080/api/shoes/${id}/`, {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  window.location.reload();
}

function ShoesList(props) {

    return(
    <div>
      <table className="table table-float table-striped">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th>Color</th>
            <th>Manufacturer</th>
            <th>Closet Name</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map(shoe => {
            return (
              <tr key={shoe.id}>
                <td><img src={shoe.photo_url} width="200" height="200"/></td>
                <td>{shoe.name}</td>
                <td>{shoe.color}</td>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.bin.closet_name}</td>
                <td><button className="btn btn-secondary" onClick={() => deleteShoes(shoe.id)} type="button">Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ShoesList;
