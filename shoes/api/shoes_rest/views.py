from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from .models import Shoe, BinVO
from common.json import ModelEncoder

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href"
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["name"]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "name",
        "color",
        "photo_url",
        "bin",
    ]
    encoders = {"bin": BinVODetailEncoder()}

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            href = content["bin"]
            bin = BinVO.objects.get(import_href=href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse (
                {"message": "Invalid Bin ID"},
                status=400
            )

    shoes = Shoe.objects.create(**content)
    return JsonResponse(
        shoes,
        encoder=ShoeDetailEncoder,
        safe=False
    )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Shoe.objects.filter(id=pk).update(**content)
        shoe = Shoe.objects.get(id=content["bin"])
        return JsonResponse (
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
