from django.db import models


class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField(null=True, blank=True)
    bin_size = models.PositiveSmallIntegerField(null=True, blank=True)
    import_href = models.CharField(max_length=200, null=True, unique=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=100)
    photo_url = models.URLField(blank=True, null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoe",
        null=True,
        blank=False,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name
